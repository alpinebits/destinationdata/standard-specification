[[anchor-agent-routes]]
==== Agent Routes

Agent routes are those used to create, retrieve, update, and delete <<anchor-agents, Agent>> resources, as well as to retrieve resources they are related to.

[cols="3,1,3,2",options="header"]
|===

| Route | Methods | Description | Additional GET features

|
  /{AlpineBitsLastRelease}/agents
  |
  GET, +
  POST
  |
  Returns a collection of all <<anchor-agents, Agent>> resources
  |
  Pagination^*M*^, +
  Sparse Fieldsets, +
  Inclusion, +
  Sorting, +
  Random Sorting, +
  Filtering, +
  Searching
|
  /{AlpineBitsLastRelease}/agents/:id
  |
  GET, +
  PATCH, +
  DELETE
  |
  Returns the <<anchor-agents, Agent>> resource identified by the `:id` parameter
  |
  Sparse Fieldsets, +
  Inclusion
|
  /{AlpineBitsLastRelease}/agents/:id
  /categories
  |
  GET
  |
  Returns the collection of <<anchor-categories, Category>> resources that classify the agent
  |
  Pagination, +
  Sparse Fieldsets, +
  Inclusion, +
  Sorting, +
  Random Sorting, +
  Filtering, +
  Searching
|
  /{AlpineBitsLastRelease}/agents/:id
  /multimediaDescriptions
  |
  GET
  |
  Returns the collection of <<anchor-mediaobjects, Media Object>> resources that are multimedia descriptions (e.g., images or videos) of the agent
  |
  Pagination, +
  Sparse Fieldsets, +
  Inclusion, +
  Sorting, +
  Random Sorting, +
  Filtering, +
  Searching
|

|===