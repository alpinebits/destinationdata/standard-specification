[[anchor-datatypes]]
== Data Types

[[anchor-address]]
=== address

An address is represented as an object containing the following fields:

* `street`: a <<anchor-text>> object containing the street name and number. +
For example: `{ "ita": "Via Bolzano 63/A", "deu": "Bozner Straße 63/A" }`

* `city`: a <<anchor-text>> object containing the city name. +
For example: `{ "ita": "Appiano sulla Strada del Vino", "deu": "Eppan an der Weinstraße" }`

* `region`: a <<anchor-text>> object containing the administrative unit in which the city is located (e.g. province, region, state). For example: `{ "ita": "Bolzano", "deu": "Bozen" }`

* `country`: a string containing a 2-letter code that identifies the country as defined in the https://www.iso.org/obp/ui/#search/code/[ISO 3166-1]. For example: `"IT"` identifies Italy. 

* `zipcode`: a string containing the zip code. For example: `"39057"`

* `complement`: a <<anchor-text>> object containing additional information about the address, such as the building name, the floor,  and/or the room number. For example: `{ "ita": "2 piano, sala 1.10", "deu": "2 etage, raum 1.10" }`

* `type`: a string containing the type of the address. For example, `"billing"`. The standard does not standardize types of addresses.

Every address object MUST have defined values for at least the `city` and `country` fields. 

Here is an example of a minimum address object:

[source,json]
----
{
  "street": null,
  "city": {
    "ita": "Appiano sulla Strada del Vino",
    "deu": "Eppan an der Weinstraße"
  },
  "region": null,
  "country": "IT",
  "zipcode": null,
  "complement": null,
  "type": null
}
----

And here is an example of a complete address object:

[source,json]
----
{
  "street": {
    "ita": "Via Bolzano 63/A",
    "deu": "Bozner Straße 63/A"
  },
  "city": {
    "ita": "Appiano sulla Strada del Vino",
    "deu": "Eppan an der Weinstraße"
  },
  "region": {
    "ita": "Bolzano",
    "deu": "Bozen"
  },
  "country": "IT",
  "zipcode": "39057",
  "complement": {
    "ita": "2 piano, sala 1.10",
    "deu": "2 etage, raum 1.10"
  },
  "type": "billing"
}
----

[[anchor-contactpoint]]
=== contact point

A contact point contains data that one can use to contact an <<anchor-agent,Agent>>. It is described by an object containing the following fields:

* `email`: an <<anchor-email>> defined for the contact point. Nullable. +
For example, `"info@alpinebits.org"`.

* `telephone`: a string containing a telephone, including the country code and without spaces, defined for the contact point. Nullable. + 
For example: `"+39000000000000"`.

* `address`: an <<anchor-address,address>> defined for the contact point. Nullable.

* `availableHours`: an array of <<anchor-hoursspecification>> objects identifying the hours in which the <<anchor-agent,Agent>> can be contacted through the means defined in the contact point. Nullable.

Every contact point object MUST define at least one of the following fields: `email`, `telephone`, `address`. 

The three objects below illustrate the minimum information that may be defined within a contact point:

.Contact point containing only an email address:
[source,json]
----
{
  "email": "info@alpinebits.org",
  "telephone": null,
  "address": null,
  "availableHours": null
}
----

.Contact point containing only a telephone:
[source,json]
----
{
  "email": null,
  "telephone": "+39000000000000",
  "address": null,
  "availableHours": null
}
----

.Contact point containing only an address:
[source,json]
----
{
  "email": null,
  "telephone": null,
  "address": {
    "street": null,
    "city": {
      "ita": "Appiano sulla Strada del Vino",
      "deu": "Eppan an der Weinstraße"
    },
    "region": null,
    "country": "IT",
    "zipcode": null
  },
  "availableHours": null
}

----

The object below illustrates the usage of all fields of a contact point object:

[source,json]
----
{
  "email": "info@alpinebits.org",
  "telephone": "+39 0471 066 600",
  "address": {
    "street": {
      "ita": "Via Bolzano 63/A",
      "deu": "Bozner Straße 63/A"
    },
    "city": {
      "ita": "Appiano sulla Strada del Vino",
      "deu": "Eppan an der Weinstraße"
    },
    "region": {
      "ita": "Bolzano",
      "deu": "Bozen"
    },
    "country": "IT",
    "zipcode": "39057"
  },
  "availableHours": {
    "dailySchedules": null,
    "weeklyScheules": [
      {
        "validFrom": "2020-01-01",
        "validTo": "2020-12-31",
        "monday": [
          { "opens": "08:00:00", "closes": "12:00:00" },
          { "opens": "14:00:00", "closes": "18:00:00" }
        ],
        "tuesday": [
          { "opens": "08:00:00", "closes": "12:00:00" },
          { "opens": "14:00:00", "closes": "18:00:00" }
        ],
        "wednesday": [
          { "opens": "08:00:00", "closes": "12:00:00" },
          { "opens": "14:00:00", "closes": "18:00:00" }
        ],
        "thursday": [
          { "opens": "08:00:00", "closes": "12:00:00" },
          { "opens": "14:00:00", "closes": "18:00:00" }
        ],
        "friday": [
          { "opens": "08:00:00", "closes": "12:00:00" },
          { "opens": "14:00:00", "closes": "18:00:00" }
        ],
        "saturday": null,
        "sunday": null
      }
    ]
  }
}
----

Note that the standard does not allow multiple emails, telephones, and addresses to be defined in a single contact point object. To exchange such data, multiple contact objects should be used.

[[anchor-date]]
=== date
A string that contains a year, a month, and a day, formatted as YYYY-MM-DD. Corresponds to _full-date_ in https://tools.ietf.org/html/rfc3339[RFC 3339]. +
Here is an example:

[source,json]
----
"2020-04-01"
----

[[anchor-datetime]]
=== date-time
A string that contains a date, a time, and a time offset (to account for time zones) and formatted as YYYY-MM-DDThh:mm:ss+hh:mm (or YYYY-MM-DDThh:mm:ss-hh:mm). Corresponds to _date-time_ in https://tools.ietf.org/html/rfc3339[RFC 3339].

The date-time that refers to April 1st, 2020 at 23 hours, 59 minutes, and 59 seconds according to the Central European Time is:

[source,json]
----
"2020-04-01T23:59:59+04:00"
----

[[anchor-email]]
=== email

A string containing a valid internet email address, as defined in  https://tools.ietf.org/html/rfc5322#section-3.4.1[RFC 5322, section 3.4.1]. 

Here is an example of a valid email:
[source,json]
----
info@alpinebits.org
----

[[anchor-geometry]]
=== geometry

The representation of geographic data structures in this standard is borrowed from the https://tools.ietf.org/html/rfc7946[GeoJSON] standard specification. 

In particular, this standard adopts GeoJSON's definition of https://tools.ietf.org/html/rfc7946#section-3.1[Geometry Object], which represents points, curves, and surfaces in coordinate space.

A generic geometry object contains the following fields:

* `type`: a string identifying the geometry type, whose possible values are `"Point"`, `"MultiPoint"`, `"LineString"`, `"MultiLineString"`, `"Polygon"`, and `"MultiPolygon"`. Non-nullable.

* `coordinates`: an array containing the coordinates of the geometry. The structure of the members of this array is determined by the type of  geometry. Non-nullable.

Definitions for each geometry type are given in the subsequent sections, being directly extracted from the https://tools.ietf.org/html/rfc7946[GeoJSON] standard specification.

[[anchor-point]]
==== point

For type `Point`, the `coordinates` member is a single position.

An example of a geometry object of type `Point` is provided below:

[source,json]
----
{
  "type": "Point",
  "coordinates": [
    11.358447074890137,
    46.49667880447103
  ]
}
----

[[anchor-multipoint]]
==== multi-point

For type `MultiPoint`, the `coordinates` member is an array of positions.

An example of a geometry object of type `MultiPoint` is provided below:

[source,json]
----
{
	"type": "MultiPoint",
	"coordinates": [
		[
			11.358447074890137,
			46.49667880447103
		],
		[
			11.119945049285889,
			46.07315619530213
		]
	]
}
----

[[anchor-linestring]]
==== line-string

For type `LineString`, the `coordinates` member is an array of two or more positions.

An example of a geometry object of type `LineString` is provided below:

[source,json]
----
{
	"type": "LineString",
	"coordinates": [
		[
			11.358447074890137,
			46.49667880447103
		],
		[
			11.119945049285889,
			46.07315619530213
		]
	]
}
----

[[anchor-multilinestring]]
==== multi-line-string

For type `MultiLineString`, the `coordinates` member is an array of LineString coordinate arrays.

An example of a geometry object of type `MultiLineString` is provided below:

[source,json]
----
{
	"type": "MultiLineString",
	"coordinates": [
		[
			[
				11.358447074890137,
				46.49667880447103
			],
			[
				11.119945049285889,
				46.07315619530213
			]
		],
		[
			[
				11.149181127548218,
				46.673201221323815
			],
			[
				11.40104055404663,
				47.263329885918694
			]
		]
	]
}
----

[[anchor-polygon]]
==== polygon

To specify a constraint specific to the `Polygon` type, it is useful to introduce the concept of a linear ring:

* A linear ring is a closed LineString with four or more positions.

* The first and last positions are equivalent, and they MUST contain identical values; their representation SHOULD also be identical.

* A linear ring is the boundary of a surface or the boundary of a hole in a surface.

* A linear ring MUST follow the right-hand rule with respect to the area it bounds, i.e., exterior rings are counterclockwise, and holes are clockwise.

Though a linear ring is not explicitly represented as a GeoJSON geometry type, it leads to a canonical formulation of the Polygon geometry type definition as follows:

* For type `Polygon`, the `coordinates` member MUST be an array of linear ring coordinate arrays.

* For Polygons with more than one of these rings, the first MUST be the exterior ring, and any others MUST be interior rings.  The exterior ring bounds the surface, and the interior rings (if present) bound holes within the surface.

An example of a geometry object of type `Polygon` is provided below:

[source,json]
----
{
  "type": "Polygon",
  "coordinates": [
    [
      [
        11.349220275878906,
        46.49902740460012
      ],
      [
        11.349080801010132,
        46.497631550714374
      ],
      [
        11.350893974304197,
        46.49759462287638
      ],
      [
        11.351033449172974,
        46.49890185307167
      ],
      [
        11.349220275878906,
        46.49902740460012
      ]
    ]
  ]
}
----

[[anchor-multipolygon]]
==== multi-polygon

For type `MultiPolygon`, the `coordinates` member is an array of Polygon coordinate arrays.

An example of a geometry object of type `MultiPolygon` is provided below:

[source,json]
----
{
  "type": "MultiPolygon",
  "coordinates": [
    [
      [
        [
          11.349220275878906,
          46.49902740460012
        ],
        [
          11.349080801010132,
          46.497631550714374
        ],
        [
          11.350893974304197,
          46.49759462287638
        ],
        [
          11.351033449172974,
          46.49890185307167
        ],
        [
          11.349220275878906,
          46.49902740460012
        ]
      ]
    ],
    [
      [
        [
          11.352425515651703,
          46.49758354452009
        ],
        [
          11.352377235889433,
          46.49728812085311
        ],
        [
          11.352744698524473,
          46.49723642154639
        ],
        [
          11.352425515651703,
          46.49758354452009
        ]
      ]
    ]
  ]
}
----

[[anchor-hoursspecification]]
=== hours specification

An hours specification object allows the representations of schedules over periods of time.
They can be used to specify when a person or organization can be contacted, when a place is open, or when a recurrent event will occur.
Hours specification objects may contain daily and weekly schedules, which in turn contain arrays of opening and closing hours for a given day.

Each opening/closing interval is represented by an object containing an `opens` and a `closes` <<anchor-time>> string.
For example, the interval from 9am to 5pm may be represented as `{ "opens": "09:00:00", "closes": "17:00:00" }`.
For a single day, an array of these objects represents the daily opening schedule.
For example, the working hours of an office may be represented by an array with two object spaced by a lunch break, as in the following case:

[source,json]
----
[
  { "opens": "09:00:00", "closes": "12:00:00" },
  { "opens": "14:00:00", "closes": "17:00:00" }
]
----

The hours specification object represents daily and weekly schedules by using the aforementioned arrays to describe the schedules of each date.
For that, the hours specification object MUST contain two nullable fields, `dailySchedules` and `weeklySchedules` that respectively contain daily and weekly schedules.

The `dailySchedules` field is a nullable non-empty object whose fields are <<anchor-date>> strings and represent the schedule for the listed dates.
Each date field is a nullable non-empty array of opening interval objects.
Each date set to `null` represents that no opening interval exists for the given date (i.e., represents a closed date).
For example, a 5-days conference containing a skipped date between satellite and main events maybe be represented as follows:

[source,json]
----
{
  "dailySchedules": {
    "2020-04-06": [ { "opens": "08:00:00", "closes": "18:00:00" } ],
    "2020-04-07": [ { "opens": "08:00:00", "closes": "18:00:00" } ],
    "2020-04-08": null,
    "2020-04-09": [ { "opens": "08:00:00", "closes": "18:00:00" } ],
    "2020-04-10": [ { "opens": "08:00:00", "closes": "18:00:00" } ],
  },
  "weeklySchedules": null
}
----

The `weeklySchedules` field is a nullable non-empty array of objects representing recurrent weekly schedules over defined periods of time.
Each object within the `weeklySchedules` array MUST contain the following fields:

* `validFrom`: a non-nullable <<anchor-date>> string that represents the first valid day of the weekly schedule.

* `validTo`: a non-nullable <<anchor-date>> string that represents the last valid day of the weekly schedule.

* `sunday`: a nullable non-empty array of opening interval objects that represents the schedule for every Sunday within the validity of the weekly schedule.

* `monday`: a nullable non-empty array of opening interval objects that represents the schedule for every Monday within the validity of the weekly schedule.

* `tuesday`: a nullable non-empty array of opening interval objects that represents the schedule for every Tuesday within the validity of the weekly schedule.

* `wednesday`: a nullable non-empty array of opening interval objects that represents the schedule for every Wednesday within the validity of the weekly schedule.

* `thursday`: a nullable non-empty array of opening interval objects that represents the schedule for every Thursday within the validity of the weekly schedule.

* `friday`: a nullable non-empty array of opening interval objects that represents the schedule for every Friday within the validity of the weekly schedule.

* `saturday`: a nullable non-empty array of opening interval objects that represents the schedule for every Saturday within the validity of the weekly schedule.

The `weeklySchedules` may represent, for example, a shopping mall that opens beyond regular hours on Saturdays but closes on Sundays, as follows:

[source,json]
----
{
  "dailySchedules": null,
  "weeklySchedules": [
    {
      "validFrom": "2020-01-01",
      "validTo": "2020-12-31",
      "monday": [ { "opens": "08:00:00", "closes": "20:00:00" } ],
      "tuesday": [ { "opens": "08:00:00", "closes": "20:00:00" } ],
      "wednesday": [ { "opens": "08:00:00", "closes": "20:00:00" } ],
      "thursday": [ { "opens": "08:00:00", "closes": "20:00:00" } ],
      "friday": [ { "opens": "08:00:00", "closes": "20:00:00" } ],
      "saturday": [ { "opens": "08:00:00", "closes": "22:00:00" } ],
      "sunday": null
    }
  ],
}
----

The following constraints apply to hours specification objects:

* within a single date, opening interval objects MUST NOT overlap

* within the `weeklySchedules` array, the validity window (between `validFrom` to `validTo`) of any two objects MUST NOT overlap

* schedules within `dailySchedules` have precedence over schedules within `weeklySchedules` for the same date

* the closing time of an opening interval object MAY go beyond midnight into the following day

To exemplify the combination daily and weekly schedules, we can include exceptions to the shopping mall example, adding that it could be closed during the first week of the year and that it includes different schedules during the holidays.

[source,json]
----
{
  "dailySchedules": {
    "2020-12-24": [ { "opens": "08:00:00", "closes": "02:00:00" } ],
    "2020-12-25": [ { "opens": "08:00:00", "closes": "16:00:00" } ],
    "2020-12-26": null,
  },
  "weeklySchedules": [
    {
      "validFrom": "2020-01-01",
      "validTo": "2020-01-07",
      "monday": null,
      "tuesday": null,
      "wednesday": null,
      "thursday": null,
      "friday": null,
      "saturday": null,
      "sunday": null
    },
    {
      "validFrom": "2020-01-08",
      "validTo": "2020-12-31",
      "monday": [ { "opens": "08:00:00", "closes": "20:00:00" } ],
      "tuesday": [ { "opens": "08:00:00", "closes": "20:00:00" } ],
      "wednesday": [ { "opens": "08:00:00", "closes": "20:00:00" } ],
      "thursday": [ { "opens": "08:00:00", "closes": "20:00:00" } ],
      "friday": [ { "opens": "08:00:00", "closes": "20:00:00" } ],
      "saturday": [ { "opens": "08:00:00", "closes": "22:00:00" } ],
      "sunday": null
    }
  ],
}
----


[[anchor-snow]]
=== snow condition

A snow condition object allows the representation of the conditions of a ski slope, a snowpark, or a mountain area at a given point in time.

The object is described by the following fields:

* `baseSnow`: a number representing the depth of base snow in a location. Measured in centimeters. Non-nullable.

* `baseSnowRange`: an object that identifies the variation in the depth of snow in a location. Nullable. This object contains two fields:
** `lower`: a number that identifies the smallest depth of snow found in a location. Measured in centimeters. Non-nullable.
** `upper`: a number that identifies the biggest depth of snow found in a location. Measured in centimeters. Non-nullable.

* `groomed`: a boolean flag indicating whether or not the snow in a location has been groomed. Nullable.

* `latestStorm`: a number representing the amount of snow from the latest storm cycle in a location. Measured in centimeters. Nullable.

* `obtainedIn`: a <<anchor-date>> or <<anchor-datetime>> string that identifies when the data contained in the snow condition objects was obtained. Nullable.

* `primarySurface`: a string that indicates the primary type of snow found in a location. Non-nullable. It is RECOMMENDED the usage of link:http://www.mtnxml.org/documentation.html#Weather[snow types] defined in MTN.XML: `"packed-powder"`, `"powder"`, `"hard-pack"`, `"loose-granular"`, `"frozen-granular""`, `"wet-packed"`, `"wet-granular"`, `"wet-snow"`, `"spring-conditions"`, `"windblown"`, `"corn-snow"`, `"icy"`, `"variable"`

* `secondarySurface`: a string that indicates the secondary type of snow found in a location. Nullable. Its recommended values are  those listed for `primarySurface`.

* `snowMaking`: a boolean flag indicating whether or not the snow in a location has been artificially produced. Nullable.

* `snowOverNight`: a number representing the amount of snow accumulated in a location throughout the night before the measurement. Measured in centimeters. Nullable.

The following example illustrates the minimal information required for a snow condition object:

[source,json]
----
{
  "obtainedIn": null, 
  "primarySurface": "powder",
  "secondarySurface": null,
  "baseSnow": 50,
  "baseSnowRange": null,
  "latestStorm": null,
  "snowOverNight": null, 
  "groomed": null,
  "snowMaking": null
}
----

The following example illustrates all fields of snow condition object:

[source,json]
----
{
  "obtainedIn": "2020-01-18T08:00:00+04:00", 
  "primarySurface": "powder",
  "secondarySurface": "packed-powder",
  "baseSnow": 50,
  "baseSnowRange": {
    "lower": 40,
    "upper": 60
  },
  "latestStorm": 40,
  "snowOverNight": 5, 
  "groomed": true,
  "snowMaking": false
}
----

[[anchor-text]]
=== text

Every field used to exchange textual data (e.g. name, description) supports the use of multiple languages. 
Such fields have, like their values, objects containing language codes as keys (e.g. `eng`, `ita`, `deu`), and the actual text in each language as values.

The example below illustrates how to represent the field `name` in German, English, and Italian.
[source,json]
----
{
  ...
  "name": {
    "deu": "Mein name",
    "eng": "My Name",
    "ita": "Il mio nome"
  },
  ...
}
----

The language codes used in the aforementioned objects are those defined in ISO 639-3, which can be found at https://iso639-3.sil.org/code_tables/639/. 

The table below contains some examples of languages and their respective ISO 639-3 codes:

[cols="1,1",options="header"]
|===

| Language | ISO-639-3

| German | `deu`
| English | `eng`
| Italian | `ita`
| Ladin | `lld`
| French | `fra`
| Chinese | `zho`
| Spanish | `spa`

|===

Note that a language-specific field should only be added if there is available data. For instance, if only the English version of a resource's name is available, it should be represented as:

[source,json]
----
{
  ...
  "name": {
    "eng": "My Name"
  },
  ...
}
----

[[anchor-time]]
=== time
A string that contains hours, minutes, and seconds, formatted as hh:mm:ss. Corresponds to _full-time_ in https://tools.ietf.org/html/rfc3339[RFC 3339].

Here is an example:

[source,json]
----
"23:59:59"
----

[[anchor-url]]
=== url

A string containing a valid universal resource identifier (URI), as defined in https://tools.ietf.org/html/rfc3986[RFC 3986]. 

Here are some valid examples provided in the RFC 3986:

```
ftp://ftp.is.co.za/rfc/rfc1808.txt

http://www.ietf.org/rfc/rfc2396.txt

ldap://[2001:db8::7]/c=GB?objectClass?one

mailto:John.Doe@example.com

news:comp.infosystems.www.servers.unix

tel:+1-816-555-1212

telnet://192.0.2.16:80/

urn:oasis:names:specification:docbook:dtd:xml:4.1.2
```



