[[anchor-agents]]
==== Agents

A resource that implements the concept of <<anchor-ontology-agent, Agent>> defined in the *{AlpineBitsOntologyR}*.

A JSON object representing such a resource MUST contain the following fields:

* `type`: the constant `"agents"` that identifies the resource as being of the type agent.

* `id`: a string that uniquely and persistently identifies the agent within a SERVER. See the definition in <<anchor-basicfields>>.

* `attributes`: an object containing the attributes of the agent. 

* `relationships`: an object containing the relationships of the agent to other resources.

* `links`: an object containing the links related to the agent.

Agent resources are structured in the following way:

[source,json]
----
{
  "type": "agents",
  "id": "1",
  "meta": { ... },
  "attributes": { ... },
  "relationships": { ... },
  "links": { ... }
}
----

===== Meta

See the definition of the `meta` object in <<anchor-metadata>>.

[[anchor-agents-attributes]]
===== Attributes

The `attributes` object of the agent resource MUST contain the following fields:

* `abstract`: a <<anchor-text>> object containing a brief description for the agent. Nullable. See the definition in <<anchor-attributes>>.

* `contactPoints`: an array of <<anchor-contactpoint>> objects. Nullable. Non-empty.

* `description`: a <<anchor-text>> object containing a description of the agent. Nullable. Conditional Assignment. See the definition in <<anchor-attributes>>.

* `name`: a <<anchor-text>> object containing the complete name of the agent. Non-nullable. Conditional Assignment. See the definition in <<anchor-attributes>>.

* `shortName`: a <<anchor-text>> object containing a short name of the agent. Nullable. See the definition in <<anchor-attributes>>. 

* `url`: a <<anchor-url>> object or string describing the agent, such as a website or a Wikipedia page. Nullable. See the definition in <<anchor-attributes>>.

A summary of the attributes is presented in the table below: 

[cols="1,1,1",options="header"]
|===

| Field | Type | Constraints

| abstract | <<anchor-text>> | Nullable
| contactPoints | Array of <<anchor-contactpoint>> | Nullable, Non-empty
| description | <<anchor-text>> | Nullable, Conditional Assignment
| name | <<anchor-url>> | Non-nullable, Conditional Assignment
| shortName | <<anchor-text>> | Nullable
| url | <<anchor-url>> | Nullable

|===

[[anchor-agents-relationships]]
===== Relationships

The `relationships` object of the agent resource MUST contain the following fields:

* `categories`: a <<anchor-referencetomany>> <<anchor-categories, category>> resources that are instantiated by the agent. See Section <<anchor-categories>>. Nullable. Non-empty.
+
The standard defines the following disjoint categories for agents:

** `"alpinebits:person"`: an agent resource representing a particular person. For instance, an artist who sings at a concert or a photographer who owns the license of a photo.

** `"alpinebits:organization"`: an agent resource representing a public or a private organization. For instance, a company organizing an event or the owner of a mountain area.

* `multimediaDescriptions`: a <<anchor-referencetomany>> media object resources (see <<anchor-mediaobjects>>) that are related to the agent. See Section <<anchor-multimediadescriptions>>. Nullable. Non-empty.

A summary of the relationships is presented in the table below:

[cols="1,1,1",options="header"]
|===

| Field | Type | Constraints

| categories | <<anchor-referencetomany>> object to <<anchor-categories>> | Nullable, Non-empty
| multimediaDescriptions | <<anchor-referencetomany>> object to <<anchor-mediaobjects>> | Nullable, Non-empty


|===

[[anchor-agents-links]]
===== Links

See the definition of the `links` object in <<anchor-links>>.

[[anchor-agents-examples]]
===== Examples

The following example presents an object containing the minimal information required of an agent resource:

[source,json]
----
include::examples/agent.min.json[]
----
|===
v|[small]**`asciidoc/examples/agent.min.json`**
|===

The following example presents an object representing an agent resource:

[source,json]
----
include::examples/agent.full.json[]
----
|===
v|[small]**`asciidoc/examples/agent.full.json`**
|===
