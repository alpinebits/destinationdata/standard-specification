[[anchor-principles]]
:sectnums:
== API Architecture

*{AlpineBitsR}* specifies a REST API designed to support the link:https://en.wikipedia.org/wiki/Client%E2%80%93server_model[client-server communication model], with a focus on system-to-system communication.

This API is built upon link:https://jsonapi.org/format/1.0/[{JSONAPI}], a specification containing best practices for the implementation of REST APIs that exchange JSON data over HTTP.

*{AlpineBitsR}* relies on its underlying transport protocol to take care of security issues. Hence *the use of HTTPS is REQUIRED*.

include::03-responsibilities.adoc[]

include::03-message.adoc[]

include::03-authentication.adoc[]

include::03-hypermedia.adoc[]
