[[anchor-mountain-area-routes]]
==== Mountain Area Routes

Mountain Area routes are those used to create, retrieve, update, and delete <<anchor-mountainareas, Mountain Area>> resources, as well as to retrieve resources they are related to.

[cols="3,1,3,2",options="header"]
|===

| Route | Methods | Description | Additional GET features

|
  /{AlpineBitsLastRelease}/mountainAreas
  |
  GET, +
  POST
  |
  Returns the collection of all <<anchor-mountainareas, Mountain Area>> resources
  |
  Pagination^*M*^, +
  Sparse Fieldsets, +
  Inclusion, +
  Sorting, +
  Random Sorting, +
  Filtering, +
  Searching
|
  /{AlpineBitsLastRelease}/mountainAreas/:id
  |
  GET, +
  PATCH, +
  DELETE
  |
  Returns the <<anchor-mountainareas, Mountain Area>> resource identified by the `id` parameter
  |
  Sparse Fieldsets, +
  Inclusion
|
  /{AlpineBitsLastRelease}/mountainAreas/:id
  /areaOwner
  |
  GET
  |
  Return the <<anchor-agents, Agent>> resource that is the owner of the mountain area
  |
  Sparse Fieldsets, +
  Inclusion
|
  /{AlpineBitsLastRelease}/mountainAreas/:id
  /categories
  |
  GET
  |
  Returns the collection of <<anchor-categories, Category>> resources that classify the mountain area
  |
  Pagination, +
  Sparse Fieldsets, +
  Inclusion, +
  Sorting, +
  Random Sorting, +
  Filtering, +
  Searching
|
  /{AlpineBitsLastRelease}/mountainAreas/:id
  /connections
  |
  GET
  |
  Returns the collection of <<anchor-lifts, Lift>>, <<anchor-mountainareas, Mountain Area>>, <<anchor-ski-slopes, Ski Slope>>, and <<anchor-snowparks, Snowpark>> resources that are physically accessible from the mountain area
  |
  Pagination, +
  Sparse Fieldsets, +
  Inclusion, +
  Sorting, +
  Random Sorting, +
  Filtering, +
  Searching
|
  /{AlpineBitsLastRelease}/mountainAreas/:id
  /lifts
  |
  GET
  |
  Returns the collection of <<anchor-lifts, Lift>> resources that are located within the mountain area
  |
  Pagination, +
  Sparse Fieldsets, +
  Inclusion, +
  Sorting, +
  Random Sorting, +
  Filtering, +
  Searching
|
  /{AlpineBitsLastRelease}/mountainAreas/:id
  /multimediaDescriptions
  |
  GET
  |
  Returns the collection of <<anchor-mediaobjects, Media Object>> resources that are multimedia descriptions (e.g., images or videos) of the mountain area
  |
  Pagination, +
  Sparse Fieldsets, +
  Inclusion, +
  Sorting, +
  Random Sorting, +
  Filtering, +
  Searching
|
  /{AlpineBitsLastRelease}/mountainAreas/:id
  /skiSlopes
  |
  GET
  |
  Returns the collection of <<anchor-ski-slopes, Ski Slope>> resources that are located within the mountain area
  |
  Pagination, +
  Sparse Fieldsets, +
  Inclusion, +
  Sorting, +
  Random Sorting, +
  Filtering, +
  Searching
|
  /{AlpineBitsLastRelease}/mountainAreas/:id
  /snowparks
  |
  GET
  |
  Returns the collection of <<anchor-snowparks, Snowpark>> resources that are located within the mountain area
  |
  Pagination, +
  Sparse Fieldsets, +
  Inclusion, +
  Sorting, +
  Random Sorting, +
  Filtering, +
  Searching
|
  /{AlpineBitsLastRelease}/mountainAreas/:id
  /subAreas
  |
  GET
  |
  Returns the collection of <<anchor-mountainareas, Mountain Area>> resources that are located within the mountain area
  |
  Pagination, +
  Sparse Fieldsets, +
  Inclusion, +
  Sorting, +
  Random Sorting, +
  Filtering, +
  Searching
|
|===