# AlpineBits DestinationData Standard

This repository contains the source files for the AlpineBits DestinationData Standard.

## Specification

The specification describes the responsibilities of DestinationData clients and servers, as well as which routes should be made available by servers, the structure of messages to be exchanged, etc.

### Generating a PDF file

To compile this documentation into an (unofficial) PDF file, you must install Asciidoctor according to your specific system (see the [instructions](https://asciidoctor.org/#installation)). With Asciidoctor installed, the following command shall generate an `AlpineBits.pdf` file:

```
asciidoctor-pdf asciidoc/AlpineBits.adoc
```

## Additional Resources

In addition to the specification itself, the AlpineBits Alliance offers a number of resources to assist parties that want to implement the standard:

- `Schemas`: for every route type defined in the standard, there is a JSON Schema that describes the structure of the messages it should return. Schemas can be used to programmatically validate messages against. 

  If you are *implementing a DestinationData server*, you can use these schemas to check if you implementation is compliant with the standard. You can also use them at run-time to check if your server is sending correctly structured messages. 

  If you are *consuming data from a DestinationData server*, you can use these schemas to validate the data you receive from it, which adds a layer of protection to your application.

  You can download all available schemas at the [DestinationData tools repository](https://gitlab.com/alpinebits/destinationdata/tools/-/tree/development/schemas).

- `Examples`: for every route type defined in the standard, there is a number of example messages. For each route, you will find two types of messages:

  *Minimum examples*: a message, identified by the `.min.json` suffix, that illustrates the minimum amount of data that can be send by a server. Required fields will have data and optional fields will be set to null. These are useful for those implementing servers to elicit a minimum set of requirements for each route and for clients to understand the least amount of data they can expect to receive.

  *Complete examples*: a message, identified by the `.full.json` suffix, that illustrates the use of all fields in a given message type.

  You can download all available examples at the [DestinationData tools repository](https://gitlab.com/alpinebits/destinationdata/tools/-/tree/development/examples).

- `Reference server implementation`: the reference implementation of an AlpineBits DestinationData server, which retrieves data from the [OpenDataHub API](http://tourism.opendatahub.bz.it/) and exposes it according to the standard. 

  An instance of the server is running at https://destinationdata.alpinebits.opendatahub.bz.it/ and can be used freely. You can access this instance through your browser or via an HTTP client like [Postman](https://www.postman.com/).


  The server's source code is available at https://github.com/noi-techpark/odh-alpinebits-destination-data-server

- `Web validator`: a single-page web app written in Javascript that offers the JSON Schema documents from DestinationData and lets users validate JSON documents against them. 

  The validators's source code is available at the [DestinationData tools repository](https://gitlab.com/alpinebits/destinationdata/tools/-/tree/development/validator).

- `Schema builder`: a javascript console app that generates JSON Schema documents describing the structure of the messages defined in the standard. Any given schema can be fed to a validator and used to programmatically check if a given message complies to it.

  The schema-builder's source code is available at the [DestinationData tools repository](https://gitlab.com/alpinebits/destinationdata/tools/-/tree/development/schema-builder).

